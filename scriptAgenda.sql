CREATE TABLE [dbo].[Contact](
	[ContactId] [int] NOT NULL PRIMARY KEY,
	[Nombre] [nvarchar](50) NULL,
	[Apellidos] [nvarchar](50) NULL,
	[Fecha_Nacimiento] [datetime] NULL,
	[Email] [nvarchar](50) NULL,
	[Telefono] [nvarchar](50) NULL,
)
GO

INSERT [dbo].[Contact] ([ContactId], [Nombre], [Apellidos], [Fecha_Nacimiento], [Email], [Telefono]) VALUES (1, N'Gabriel', N'Tecu', '08/17/1980', N'Gaby0Cool@yahoo.es', N'+34644344992')
INSERT [dbo].[Contact] ([ContactId], [Nombre], [Apellidos], [Fecha_Nacimiento], [Email], [Telefono]) VALUES (2, N'Sergio', N'Torres López', '10/10/1985', N'sergio123@yahoo.com', N'+16548953464')
INSERT [dbo].[Contact] ([ContactId], [Nombre], [Apellidos], [Fecha_Nacimiento], [Email], [Telefono]) VALUES (3, N'Diana', N'Paola Pardo', '10/08/1983', N'diana123@lenoid.net', N'+34657466445')
INSERT [dbo].[Contact] ([ContactId], [Nombre], [Apellidos], [Fecha_Nacimiento], [Email], [Telefono]) VALUES (4, N'Lucia', N'Ramos García', '07/04/1949', N'lucia123@gmail.com', N'+71524165121')
INSERT [dbo].[Contact] ([ContactId], [Nombre], [Apellidos], [Fecha_Nacimiento], [Email], [Telefono]) VALUES (5, N'Adrian', N'Serrano Ramos', '11/05/1956', N'adrian123@yahoo.es', N'+48652164123')
INSERT [dbo].[Contact] ([ContactId], [Nombre], [Apellidos], [Fecha_Nacimiento], [Email], [Telefono]) VALUES (6, N'Antonio', N'Bellón Agudo', '12/07/1993', N'antonio123@lenoid.net', N'+96452121230')
INSERT [dbo].[Contact] ([ContactId], [Nombre], [Apellidos], [Fecha_Nacimiento], [Email], [Telefono]) VALUES (7, N'Oscar', N'Ford Fairlane', '04/04/1976', N'oscar123@gmail.com', N'+34659824230')
INSERT [dbo].[Contact] ([ContactId], [Nombre], [Apellidos], [Fecha_Nacimiento], [Email], [Telefono]) VALUES (8, N'Pedro', N'Casales', '02/11/1979', N'pedro123@yahoo.com', N'+56954656120')
INSERT [dbo].[Contact] ([ContactId], [Nombre], [Apellidos], [Fecha_Nacimiento], [Email], [Telefono]) VALUES (9, N'Luis', N'Alcazar Robledo', '04/05/1976', N'luis123@lenoid.net', N'+46165136546')

SELECT * FROM Contact