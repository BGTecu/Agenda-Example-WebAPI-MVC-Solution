﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agenda_Example_API.Models
{
    public class ContactModel
    {
        AgendaContext ctx;

        public ContactModel()
        {
            this.ctx = new AgendaContext();
        }

        //GET api/Contacts
        public List<Contact> GetContacts()
        {
            var query = from data in ctx.Contact select data;
            return query.ToList();
        }

        //GET api/Contacts/{id}
        public Contact GetContactByID(int cID)
        {
            Contact c = ctx.Contact.Find(cID);
            return c;
        }

        //METODO POST
        public void AddContact(Contact c)
        {
            int lastID = ctx.Contact.OrderByDescending(z => z.ContactId).FirstOrDefault().ContactId;
            c.ContactId = lastID + 1;
            this.ctx.Contact.Add(c);
            this.ctx.SaveChanges();
        }

        //METODO PUT
        public void EditContact(Contact c)
        {
            Contact contact = this.GetContactByID(c.ContactId);
            contact.Nombre = c.Nombre;
            contact.Apellidos = c.Apellidos;
            contact.Email = c.Email;
            contact.Telefono = c.Telefono;
            this.ctx.SaveChanges();
        }

        //METODO DELETE
        public void DeleteContact(int cID)
        {
            Contact c = this.GetContactByID(cID);
            this.ctx.Contact.Remove(c);
            this.ctx.SaveChanges();
        }
    }
}