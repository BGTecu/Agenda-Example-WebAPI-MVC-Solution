﻿using Agenda_Example_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Agenda_Example_API.Controllers
{
    public class ContactsController : ApiController
    {
        ContactModel model;
        public ContactsController()
        {
            this.model = new ContactModel();
        }

        // GET: api/Contacts
        public List<Contact> Get()
        {
            return model.GetContacts();
        }

        // GET: api/Contacts/5
        public Contact Get(int id)
        {
            return model.GetContactByID(id);
        }

        // POST: api/Contacts
        public void Post(Contact c)
        {
            this.model.AddContact(c);
        }

        // PUT: api/Contacts/5
        public void Put(Contact c)
        {
            this.model.EditContact(c);
        }

        // DELETE: api/Contacts/5
        public void Delete(int id)
        {
            this.model.DeleteContact(id);
        }
    }
}
