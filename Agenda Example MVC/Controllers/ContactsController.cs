﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Threading.Tasks;
using Agenda_Example_MVC.Models;

namespace Agenda_Example_MVC.Controllers
{
    public class ContactsController : Controller
    {
        ContactModel model;
        public ContactsController()
        {
            this.model = new ContactModel();
        }

        // GET: Contacts
        [AsyncTimeout(1000)]
        public async Task<ActionResult> Index()
        {
            List<Contact> cList = await model.GetContacts();
            return View(cList);
        }

        // GET: Contacts/Details/5
        public async Task<ActionResult> Details(int id)
        {
            Contact c = await model.GetContactByID(id);
            return View(c);
        }

        // GET: Contacts/Create
        public async Task<ActionResult> Create()
        {
            return View();
        }

        // POST: Contacts/Create
        [HttpPost]
        public async Task<ActionResult> Create(Contact c)
        {
            try
            {
                await model.AddContact(c);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Contacts/Edit/5
        public async Task<ActionResult> Edit(int id)
        {
            Contact c = await model.GetContactByID(id);
            return View(c);
        }

        // POST: Contacts/Edit/5
        [HttpPost]
        public async Task<ActionResult> Edit(Contact c)
        {
            try
            {
                await model.EditContact(c);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Contacts/Delete/5
        public async Task<ActionResult> Delete(int id)
        {
            await model.DeleteContact(id);
            return RedirectToAction("Index");
        }
    }
}
