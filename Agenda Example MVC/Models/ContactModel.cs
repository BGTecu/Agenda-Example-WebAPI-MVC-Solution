﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Agenda_Example_MVC.Models
{
    public class ContactModel
    {
        private String UriApi;
        MediaTypeWithQualityHeaderValue mediaheader;
        public ContactModel()
        {
            //this.UriApi = "http://localhost:50809/"; // Local API
            this.UriApi = "http://agendaexampleapi.azurewebsites.net/"; // Azure API
            this.mediaheader = new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json");
        }

        public async Task<List<Contact>> GetContacts()
        {
            using (HttpClient client = new HttpClient())
            {
                String petition = "api/Contacts";
                client.BaseAddress = new Uri(this.UriApi);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(mediaheader);
                HttpResponseMessage respuesta = await client.GetAsync(petition);
                if (respuesta.IsSuccessStatusCode)
                {
                    List<Contact> cList = await respuesta.Content.ReadAsAsync<List<Contact>>();
                    return cList;
                }
                else { return null; }
            }
        }

        public async Task<Contact> GetContactByID(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                String petition = "api/Contacts/" + id;
                client.BaseAddress = new Uri(this.UriApi);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(mediaheader);
                HttpResponseMessage respuesta = await client.GetAsync(petition);
                if (respuesta.IsSuccessStatusCode)
                {
                    Contact c = await respuesta.Content.ReadAsAsync<Contact>();
                    return c;
                }
                else { return null; }
            }
        }

        public async Task AddContact(Contact c)
        {
            using (HttpClient client = new HttpClient())
            {
                String peticion = "api/Contacts";
                client.BaseAddress = new Uri(this.UriApi);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(mediaheader);
                await client.PostAsJsonAsync(peticion, c);
            }
        }

        public async Task EditContact(Contact c)
        {
            using (HttpClient client = new HttpClient())
            {
                String peticion = "api/Contacts";
                client.BaseAddress = new Uri(this.UriApi);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(mediaheader);
                await client.PutAsJsonAsync(peticion, c);
            }
        }

        public async Task DeleteContact(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                String peticion = "api/Contacts/" + id;
                client.BaseAddress = new Uri(this.UriApi);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(mediaheader);
                await client.DeleteAsync(peticion);
            }
        }
    }
}