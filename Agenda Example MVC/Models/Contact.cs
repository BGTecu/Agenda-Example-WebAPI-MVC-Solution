﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agenda_Example_MVC.Models
{
    public class Contact
    {
        public int ContactId { get; set; }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public Nullable<System.DateTime> Fecha_Nacimiento { get; set; }
        public string Email { get; set; }
        public string Telefono { get; set; }
    }
}